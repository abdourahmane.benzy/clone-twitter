import React from 'react';
import logo from './twitter.png'
import  './Twitter.css';
import google from './google.png';
import apple from './apple.png'
import images from './images.jpeg'


const Twitter = ()=>{
    return(

        <div className='container-fluid hauteur'>

            <div className='row d-flex' style={{marginTop:'0%'}}>
                <div className='col-4 bgTwit  mr-4' style={{paddingRight:'40%', paddingBottom:'10%'}}>
                        <img src={logo} align='left' />
                </div>
                
                
                <div className=' col m-0 ' style={{marginLeft:'-10%'}}>

                    <div className=' p-0 m-0 w-25'>
                        <img src={logo} height={30} /> 
                    </div>

                    <div className="row d-block   w-75 mt-5" bg-light novalidate style={{marginRight:'20px', borderRadius:'5%'}}>
                      <h1><span className='App-header'>Ça se passe maintenant</span></h1>
                    </div>

                    <div className="row d-block w-100 mb-4" bg-light novalidate style={{marginRight:'20px', borderRadius:'5%'}}>
                      <span className='App-header2'>Rejoignez Twitter dès aujourd'hui.</span>
                    </div>

                   
                        <a className="btns mb-2" href="#">
                        <img src={google} alt="" srcset="" height={30}/> S'inscrire avec google
                        </a>

                        <a className="btns mb-2" href="#">
                        <img src={apple} alt="" srcset="" height={32}/> S'inscrire avec apple
                        </a>
{/* 
                        <a className="btns mb-2" href="#">
                        <img src={google} alt="" srcset="" height={30}/> S'inscrire avec google
                        </a> */}

                        <a className="btnTel " href="#">
                        S'inscrire avec numero de télépho...
                        </a>
                   

                   
                       
                       <div className='row  w-75 textCond mt-4'>
                            {/* <p className='text-break' style={{fontSize:'17.2px'}}> */}
                                En vous inscrivant, vous acceptez les &nbsp;<a href="http://"> Conditions d'Utilisation </a> et
                                la Politique de Confidentialité, incluant&nbsp;<a href="http://"> l'Utilisation de Cookies. </a> 
                                &nbsp;<br /> <br />
                                <p>Vous avez déjà un compte ? <a href="http://"> Connectez-vous</a></p>
                            {/* </p>      */}
                      </div>  


                       
                </div>
            </div>

            <div className='row  ' style={{marginLeft:'1.4%'}}>
                <p className='textFoot'>
                    <a href="http://" target="_blank" rel="noopener noreferrer" className='pr-4'>A propos</a>
                    <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Centre d'assistance</a>
                    <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Condition d'utilisation</a>
                    <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Politique de condidentialité</a>
                    <a href="http://" target="_blank" rel="noopener noreferrer" className='pr-4'>Politique relative aux cookies</a>
                    <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Information sur les publicités</a>
                    <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Blog</a>
                    <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Status</a>
                    <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Carrières</a>
                    <a href="http://" target="_blank" rel="noopener noreferrer" className='pr-4'>Publicités</a> <br /><br />

                    <span  style={{marginLeft:'7.8%'}}>
                
                        <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4' style={{marginLeft:'19%'}}>Twitter pour les professionels</a>
                        <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Développeur</a>
                        <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Répertoire</a>
                        <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>Paramètres</a>
                        <a href="http://" target="_blank" rel="noopener noreferrer"  className='pr-4'>© 2021 Twitter, Inc.</a>
                        
                    </span>
                </p>
            </div>
       
        </div>

    )
}
export default Twitter;